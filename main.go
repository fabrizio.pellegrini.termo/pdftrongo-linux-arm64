package main

import . "github.com/pdftron/pdftron-go/v2"

func main() {
	PDFNetInitialize("demo:1683723412816:7dd0ee29030000000085cc02be3a74d6ebeb2e520f062fe3f724fd0461")
	doc := NewPDFDoc("template1.pdf")

	doc.InitSecurityHandler()
	defer doc.Close()

	replacer := NewContentReplacer()
	page := doc.GetPage(1)

	// replace an image on the page
	targetRegion := page.GetMediaBox()
	img := ImageCreate(doc.GetSDFDoc(), "img1.png")
	replacer.AddImage(targetRegion, img.GetSDFObj())
	replacer.Process(page)

	doc.Save("pdfapikey_result.pdf", uint(SDFDocE_remove_unused))
}
