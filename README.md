# Pdftron-go 2.2.6 linux arm64 Test

The purpose of this repository is to test the new pdftron-go version (2.2.6) on a linux arm64 container.
At the moment, the build process fails. Something related to the dynamic linking of the CGO library.
This testing project does only one thing. It takes the file called "template1.pdf", that contains an image, and replace it with a new image (img1.png). It works fine locally by using either ```go run .``` or ```go build```, but I still can't build the project by using Docker and a linux/arm64 based image.

## Build

```
docker build --progress=plain --no-cache --tag pdfgen
```

## Run

```
docker run --rm -it pdfgen
(not working since the image build fails)
```
