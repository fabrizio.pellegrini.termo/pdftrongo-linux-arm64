FROM golang:alpine3.19

WORKDIR /app

RUN apk update && apk upgrade
RUN apk --no-cache add build-base

RUN uname
RUN uname -m

RUN go env -w CGO_ENABLED=1
RUN go env -w GOOS=linux
RUN go env -w GOARCH=arm64

RUN go env

COPY go.mod go.sum ./

COPY *.go ./

RUN go mod download

RUN go build -x -o /pdfgen

EXPOSE 8080

CMD ["/pdfgen"]
